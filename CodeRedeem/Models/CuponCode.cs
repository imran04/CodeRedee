﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CodeRedeem.Models
{
    public class CouponCode
    {
        public Guid Id { get; set; }
        public string Sku { get; set; }
        [EnumDataType(typeof(TypeOfCoupon))]
        public TypeOfCoupon CouponType { get; set; }
        public string Code { get; set; }
        public string UserId { get; set; }
        public bool Active { get; set; }
        public DateTimeOffset CreatedOn { get; set; }
        public DateTimeOffset? RedeemOn { get; set; }
    }

    public enum TypeOfCoupon
    {
        Cash,// Dealer, mhech
        Promo// Distributer dealer
    }
}
