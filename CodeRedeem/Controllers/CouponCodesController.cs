﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CodeRedeem.Data;
using CodeRedeem.Models;

namespace CodeRedeem.Controllers
{
    public class CouponCodesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public CouponCodesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: CouponCodes
        public async Task<IActionResult> Index(int page=0,int size=20)
        {
            return View(await _context.CouponCodes.OrderBy(i=>i.CreatedOn).Skip(page*size).Take(size).ToListAsync());
        }


        public IActionResult QrCodeGrid(int page=0,int size = 10)
        {
            return View(_context.CouponCodes.OrderBy(i => i.CreatedOn).Skip(page * size).Take(size).ToList());
        }
        // GET: CouponCodes/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var couponCode = await _context.CouponCodes
                .SingleOrDefaultAsync(m => m.Id == id);
            if (couponCode == null)
            {
                return NotFound();
            }

            return View(couponCode);
        }

        // GET: CouponCodes/Create
        public IActionResult Create()
        {
            return View(new CouponCode { Code = CodeGenrator.GenrateReferCode() });
        }

        // POST: CouponCodes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Sku,CouponType,Code,UserId,Active,CreatedOn,RedeemOn")] CouponCode couponCode)
        {
            if (ModelState.IsValid)
            {
                couponCode.Id = Guid.NewGuid();
                _context.Add(couponCode);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(couponCode);
        }

        // GET: CouponCodes/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var couponCode = await _context.CouponCodes.SingleOrDefaultAsync(m => m.Id == id);
            if (couponCode == null)
            {
                return NotFound();
            }
            return View(couponCode);
        }

        // POST: CouponCodes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("Id,Sku,CouponType,Code,UserId,Active,CreatedOn,RedeemOn")] CouponCode couponCode)
        {
            if (id != couponCode.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(couponCode);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CouponCodeExists(couponCode.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(couponCode);
        }

        // GET: CouponCodes/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var couponCode = await _context.CouponCodes
                .SingleOrDefaultAsync(m => m.Id == id);
            if (couponCode == null)
            {
                return NotFound();
            }

            return View(couponCode);
        }

        // POST: CouponCodes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var couponCode = await _context.CouponCodes.SingleOrDefaultAsync(m => m.Id == id);
            _context.CouponCodes.Remove(couponCode);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CouponCodeExists(Guid id)
        {
            return _context.CouponCodes.Any(e => e.Id == id);
        }
    }
    //Class to genrate Code
    public class CodeGenrator
    {
        public static string GenrateReferCode()
        {
            var opts = new
            {
                RequiredLength = 12,
                RequiredUniqueChars = 8,
                RequireDigit = true,
                RequireLowercase = false,
                RequireNonAlphanumeric = false,
                RequireUppercase = true
            };

            string[] randomChars = new[] {
                "ABCDEFGHJKLMNOPQRSTUVWXYZ",    // uppercase 
                "abcdefghijkmnopqrstuvwxyz",    // lowercase
                "0123456789"                  // digits
                                      // non-alphanumeric
            };
            Random rand = new Random(Environment.TickCount);
            List<char> chars = new List<char>();

            if (opts.RequireUppercase)
                chars.Insert(rand.Next(0, chars.Count),
                    randomChars[0][rand.Next(0, randomChars[0].Length)]);

            if (opts.RequireLowercase)
                chars.Insert(rand.Next(0, chars.Count),
                    randomChars[1][rand.Next(0, randomChars[1].Length)]);

            if (opts.RequireDigit)
                chars.Insert(rand.Next(0, chars.Count),
                    randomChars[2][rand.Next(0, randomChars[2].Length)]);

            //if (opts.RequireNonAlphanumeric)
            //    chars.Insert(rand.Next(0, chars.Count),
            //        randomChars[3][rand.Next(0, randomChars[3].Length)]);

            for (int i = chars.Count; i < opts.RequiredLength
                || chars.Distinct().Count() < opts.RequiredUniqueChars; i++)
            {
                string rcs = randomChars[rand.Next(0, randomChars.Length)];
                chars.Insert(rand.Next(0, chars.Count),
                    rcs[rand.Next(0, rcs.Length)]);
            }

            return new string(chars.ToArray()).ToUpper();
        }
    }
}
